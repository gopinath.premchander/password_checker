import re
def password_checker():
    pwd = input('Enter a password: ')
    c1 = len(re.findall('[A-Z]', pwd))
    c2 = len(re.findall('[a-z]', pwd))
    c3 = len(re.findall('[0-9]', pwd))
    c4 = len(re.findall('[#@$]', pwd))
    
    if len(pwd) >=8 and len(pwd) <=14:
        if c1>=2 and c2>=3 and c3>=2 and c4>=1:
            print('Password is valid')
        else:
            print('Password is invalid')
        
    else:
        print('Password is invalid')