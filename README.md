Write a function to get a password as input from the user and check the following conditions to validate the password. If the password satisfies all the conditions then print its a valid password otherwise print invalid password.

Conditions:
1. It should contain greater than or equal to 8 characters and less than 14 characters.
2. It should contain at least 2 capital letters.
3. it should contain at least 3 small letters.
4. it should contain at least 2 number
5. it should contain any one of these special characters #@$